//
//  AgentProfile.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/2/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

struct ActiveListing {
    var image: UIImage
    var name: String
    var location: String
    var price: String
}

struct RatingReview {
    var name: String
    var date: String
    var review: String
    var star: String
}

struct Rating {
    var isExpandable: Bool
    var rating: [RatingReview]
}


class AgentProfile: UIViewController {
    
    
    var name: String?
    var image: UIImage?
    
    let data = [ActiveListing(image: #imageLiteral(resourceName: "home"), name: "3 chome 20th Ave", location: "Shiho city, BC V5Y 2C7, Tokyo", price: "230,000,000"),
    ActiveListing(image: #imageLiteral(resourceName: "home"), name: "3 chome 20th Ave", location: "Shiho city, BC V5Y 2C7, Tokyo", price: "230,000,000")
    ]
    
    var ratingData = [Rating(isExpandable: true, rating: [RatingReview(name: "Shun Oguri", date: "25 July 2019", review: "I would say this is good value for money. Also very close to the Old Town (history museum etc). There didnt seem to be much atmosphere but it was clean and not noisy or too crowded.", star: "5"),
    RatingReview(name: "RaaShun Oguri", date: "7 August 2019", review: "I would say this is good value for money. Also very close to the Old Town (history museum etc). There didnt seem to be much atmosphere but it was clean and not noisy or too crowded.", star: "5")
    ])]
    
    var moreLessToggle: Bool = false
    var toggleAnimationDirection: Bool = true
    
    let sendMsgButton = CustomButton(buttonName: "Send Message", bgColor: #colorLiteral(red: 0.1685836613, green: 0.3353172839, blue: 0.7531601191, alpha: 1), textColor: .white)
    let makeCallButton = CustomButton(buttonName: "Make a call", bgColor: #colorLiteral(red: 0.2501999438, green: 0.7749417424, blue: 0.7332472801, alpha: 1), textColor: .white)
    
    lazy var scrollView: UIScrollView = {
       let sv = UIScrollView()
        sv.bounces = false
        sv.showsVerticalScrollIndicator = false
        return sv
    }()
    
    
    lazy var profileImage: UIImageView = {
        let iv = UIImageView(image: image)
        iv.layer.cornerRadius = 50
        iv.clipsToBounds = true
        return iv
    }()
    
    lazy var agentName: UILabel = {
       let label = UILabel()
        label.text = name
        return label
    }()
    
    lazy var agentCategory: UILabel = {
       let label = UILabel()
        label.text = "Leader of JPN real estate group"
        return label
    }()
    
    lazy var stackView: UIStackView = {
       let sv = UIStackView()
        sv.axis = .horizontal
        sv.distribution = .fill
        sv.spacing = 4
        let starImage = UIImageView(image: UIImage(named: "star"))
        starImage.contentMode = .scaleAspectFit

        let label = UILabel()
     
        label.text = "5/5"

        let reviewLabel = UILabel()
        reviewLabel.text = "(20 reviews)"
        reviewLabel.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)

        sv.addArrangedSubview(starImage)
        starImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            starImage.heightAnchor.constraint(equalToConstant: 15),
            starImage.widthAnchor.constraint(equalToConstant: 15)
        ])

        sv.addArrangedSubview(label)
        sv.addArrangedSubview(reviewLabel)
       
        return sv
    }()
    
    lazy var buttonStackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        sv.spacing = 10
        sv.distribution = .fillEqually
        sv.addArrangedSubview(sendMsgButton)
        sv.addArrangedSubview(makeCallButton)
        return sv
    }()
    
    lazy var aboutMeLabel: UILabel = {
       let label = UILabel()
        label.text = "About me"
        
        return label
    }()
    
    lazy var descriptionLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 3
        label.textAlignment = .justified
        let attr = NSMutableAttributedString(string: "I began my professional carrer in investment real estate in 2004 on Tokyo. I bring the experience of the business, maerketing and personal dimension of the earth and it is very necessary")
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.2
        attr.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attr.length))
        label.attributedText = attr
        return label
    }()
    
    lazy var moreButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("More", for: .normal)
        button.addTarget(self, action: #selector(showContent), for: .touchUpInside)
        return button
    }()
    

    @objc func showContent(sender: UIButton) {
       
        moreLessToggle = !moreLessToggle
        descriptionLabel.numberOfLines = moreLessToggle ? 0 : 3
        moreButton.setTitle(moreLessToggle ? "Less" : "More", for: .normal)

    }
    
    lazy var tableView1: UITableView = {
       let tv = UITableView()
        tv.separatorStyle = .none
       return tv
    }()
    
    lazy var ratingTableView: UITableView = {
       let tv = UITableView()
       tv.separatorStyle = .none
        return tv
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Agent Profile"
        configureScrollView()
        configureProfileImage()
        configureAgentName()
        configureAgentCategory()
        configureStackview()
        configureButtonStackView()
        configureAboutMe()
        configureDescriptionLabel()
        configureMoreButton()
        configureTableview1()
        configureRatingTableView()
    }
    
    func configureScrollView() {
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    
    func configureProfileImage() {
        scrollView.addSubview(profileImage)
        profileImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            profileImage.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 10),
            profileImage.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            profileImage.heightAnchor.constraint(equalToConstant: 100),
            profileImage.widthAnchor.constraint(equalToConstant: 100)
        ])
    }
    
    func configureAgentName() {
        scrollView.addSubview(agentName)
        agentName.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            agentName.topAnchor.constraint(equalTo: profileImage.bottomAnchor, constant: 10),
            agentName.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
        ])
    }
    
    func configureAgentCategory() {
        scrollView.addSubview(agentCategory)
        agentCategory.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            agentCategory.topAnchor.constraint(equalTo: agentName.bottomAnchor, constant: 5),
            agentCategory.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor)
        ])
    }
    
    func configureStackview() {
        scrollView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: agentCategory.bottomAnchor, constant: 8),
            stackView.heightAnchor.constraint(equalToConstant: 40),
            stackView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            stackView.widthAnchor.constraint(equalToConstant: 150)
        ])
    }
    
    func configureButtonStackView() {
        scrollView.addSubview(buttonStackView)
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            buttonStackView.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 5),
            buttonStackView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            buttonStackView.widthAnchor.constraint(equalToConstant: view.frame.width - 30),
            buttonStackView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func configureAboutMe() {
        scrollView.addSubview(aboutMeLabel)
        aboutMeLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            aboutMeLabel.topAnchor.constraint(equalTo: buttonStackView.bottomAnchor, constant: 10),
            aboutMeLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor)
        ])
    }
    
    func configureDescriptionLabel() {
        scrollView.addSubview(descriptionLabel)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: aboutMeLabel.bottomAnchor, constant: 5),
            descriptionLabel.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            descriptionLabel.widthAnchor.constraint(equalToConstant: view.frame.width - 30)
        ])
    }
    
    func configureMoreButton() {
        scrollView.addSubview(moreButton)
        moreButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            moreButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 5),
            moreButton.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor)
        ])
    }
    
    func configureTableview1() {
        scrollView.addSubview(tableView1)
        tableView1.delegate = self
        tableView1.dataSource = self
        tableView1.register(ActiveListingCell.self, forCellReuseIdentifier: "activeListing")
        tableView1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView1.topAnchor.constraint(equalTo: moreButton.bottomAnchor, constant: 10),
            tableView1.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            tableView1.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            tableView1.heightAnchor.constraint(equalToConstant: 230),
    
        ])
    }
    
    func configureRatingTableView() {
        scrollView.addSubview(ratingTableView)
        ratingTableView.translatesAutoresizingMaskIntoConstraints = false
        ratingTableView.delegate = self
        ratingTableView.dataSource = self
        ratingTableView.register(RatingTableCell.self, forCellReuseIdentifier: "ratingCell")
        NSLayoutConstraint.activate([
            ratingTableView.topAnchor.constraint(equalTo: tableView1.bottomAnchor, constant: 15),
            ratingTableView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            ratingTableView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            ratingTableView.heightAnchor.constraint(equalToConstant: 500),
            ratingTableView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -15)
        ])
    }
    
}

extension AgentProfile: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView1 {
             return data.count
        }
        else {

            if !ratingData[section].isExpandable {
                return 0
            }
            else {
                return ratingData[section].rating.count            }
        
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableView1 {
            let cell = tableView1.dequeueReusableCell(withIdentifier: "activeListing", for: indexPath) as! ActiveListingCell
            cell.setData(image: data[indexPath.row].image, name: data[indexPath.row].name, location: data[indexPath.row].location, price: data[indexPath.row].price)
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell = ratingTableView.dequeueReusableCell(withIdentifier: "ratingCell", for: indexPath) as! RatingTableCell
            cell.setData(name: ratingData[indexPath.section].rating[indexPath.row].name, date: ratingData[indexPath.section].rating[indexPath.row].date, description: ratingData[indexPath.section].rating[indexPath.row].review)
            cell.selectionStyle = .none
            return cell
        }

      
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        
        if tableView == tableView1 {
            let view = UIView()
            
            let label = UILabel()
            label.text = "Active Listing (\(data.count))"
            label.font = UIFont.systemFont(ofSize: 14)
            view.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                label.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
            return view
        }
        else {
            let view = UIView()
            
            let label = UILabel()
            label.text = "Rating and Review"
            label.font = UIFont.systemFont(ofSize: 14)
            view.addSubview(label)
            
            label.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
                label.leadingAnchor.constraint(equalTo: view.leadingAnchor)
            ])
            
            let upArrow = UIButton(type: .system)
            upArrow.setImage(#imageLiteral(resourceName: "downarrow"), for: .normal)
            upArrow.imageView?.contentMode = .scaleAspectFit
            upArrow.addTarget(self, action: #selector(toggleTable), for: .touchUpInside)
            upArrow.tag = section
            view.addSubview(upArrow)
            upArrow.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                upArrow.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -5),
                upArrow.topAnchor.constraint(equalTo: view.topAnchor, constant: 10),
                upArrow.heightAnchor.constraint(equalToConstant: 15),
                upArrow.widthAnchor.constraint(equalToConstant: 15)
            ])
            
            let label1 = UILabel()
            label1.text = "4.6 /5"
            label1.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
            label1.font = UIFont.systemFont(ofSize: 25)
            

            view.addSubview(label1)
            label1.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                label1.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                label1.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
            
            let reviewLabel = UILabel()
            reviewLabel.text = "\(ratingData[section].rating.count) Reviews"
            reviewLabel.font = UIFont.systemFont(ofSize: 14)
            view.addSubview(reviewLabel)
            reviewLabel.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                reviewLabel.topAnchor.constraint(equalTo: label1.bottomAnchor, constant: 7),
                reviewLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            ])
            return view
        }
        
    }
    
    @objc func toggleTable(sender: UIButton) {
        let sectionCount = sender.tag
        var indexPaths = [IndexPath]()
        
        for a in ratingData[sectionCount].rating.indices {
            let indexPath = IndexPath(item: a, section: sectionCount)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = ratingData[sectionCount].isExpandable
        ratingData[sectionCount].isExpandable = !isExpanded

        
        if isExpanded {
            ratingTableView.deleteRows(at: indexPaths, with: .fade)
          
        }
        else {
            ratingTableView.insertRows(at: indexPaths, with: .fade)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tableView1 {
               return 30
        }
        else {
            return 120
        }
    
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == tableView1 {
            return 1
        }
        else {
            return ratingData.count
        }
     
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableView1 {
            return 90
        }
        else {
            return 130
        }
      
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if(tableView == ratingTableView) {
                  let view = UIView()
                   let button = UIButton(type: .system)
                   button.setTitle("Write a review", for: .normal)
                   button.tintColor = #colorLiteral(red: 0.2501999438, green: 0.7749417424, blue: 0.7332472801, alpha: 1)
                   button.layer.cornerRadius = 20
                   button.layer.borderWidth = 1
                   button.layer.borderColor = #colorLiteral(red: 0.2501999438, green: 0.7749417424, blue: 0.7332472801, alpha: 1)
                   view.addSubview(button)
            
                   button.translatesAutoresizingMaskIntoConstraints = false
                   NSLayoutConstraint.activate([
                       button.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                       button.heightAnchor.constraint(equalToConstant: 40),
                       button.trailingAnchor.constraint(equalTo: view.trailingAnchor)
                   ])
                   return view
        }
        
        else{
            return view
        }
        
 
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView == ratingTableView {
            return 50
        }
        else {
            return 0
        }
     
    }
}



