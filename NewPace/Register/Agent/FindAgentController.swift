//
//  FindAgentController.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/2/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class FindAgentController: UIViewController {
    
      lazy var searchBar: UIView = {
    
          let sb = UIView()
          let innerView = UIView()
          
          sb.layer.cornerRadius = 6
          sb.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.968627451, blue: 0.9882352941, alpha: 1)
          
          let tf = UITextField()
          tf.placeholder = "Enter agent name"
          
          innerView.addSubview(tf)
          tf.translatesAutoresizingMaskIntoConstraints = false
          
          NSLayoutConstraint.activate([
              tf.heightAnchor.constraint(equalTo: innerView.heightAnchor),
              tf.centerYAnchor.constraint(equalTo: innerView.centerYAnchor),
              tf.leadingAnchor.constraint(equalTo: innerView.leadingAnchor, constant: 45),
              tf.trailingAnchor.constraint(equalTo: innerView.trailingAnchor)
              
          ])

        let searchImage = UIImageView(image: UIImage(named: "search"))
          searchImage.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
          searchImage.image = searchImage.image?.withTintColor(.gray)
          innerView.addSubview(searchImage)
          searchImage.translatesAutoresizingMaskIntoConstraints = false
          NSLayoutConstraint.activate([
              searchImage.trailingAnchor.constraint(equalTo: tf.leadingAnchor, constant: -5),
              searchImage.centerYAnchor.constraint(equalTo: innerView.centerYAnchor)
          ])
          
          sb.addSubview(innerView)
          innerView.translatesAutoresizingMaskIntoConstraints = false
          NSLayoutConstraint.activate([
              innerView.topAnchor.constraint(equalTo: sb.topAnchor),
              innerView.leadingAnchor.constraint(equalTo: sb.leadingAnchor),
              innerView.bottomAnchor.constraint(equalTo: sb.bottomAnchor),
              innerView.trailingAnchor.constraint(equalTo: sb.trailingAnchor)
          ])
          return sb
      }()
    
    lazy var searchAreaView: UIView = {
       let view = UIView()
        let label = UILabel()
        label.text = "Search area"
  
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: view.topAnchor),
            label.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
        
        let label1 = UILabel()
        label1.text = "Browse agent by area"
        view.addSubview(label1)
        label1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
          
            label1.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            label1.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10)
        ])
        
        let rightArrow = UIImageView()
        rightArrow.image = UIImage(named: "rightArrow")
        rightArrow.contentMode = .scaleAspectFit
        rightArrow.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        view.addSubview(rightArrow)
        rightArrow.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            rightArrow.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            rightArrow.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10)
        ])
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Find an agent"
        configureSearchBar()
        configureSearchAreaView()
    }
    
    func configureSearchBar() {
        view.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
            searchBar.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func configureSearchAreaView() {
        view.addSubview(searchAreaView)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(navToSearchAgent))
        searchAreaView.addGestureRecognizer(gesture)
        searchAreaView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            searchAreaView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 20),
            searchAreaView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            searchAreaView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
            searchAreaView.heightAnchor.constraint(equalToConstant: 60)
        ])
    }
    @objc func navToSearchAgent() {
            self.navigationController?.pushViewController(FindAgentForm(), animated: true)

    }
    
}
