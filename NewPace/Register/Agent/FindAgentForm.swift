//
//  FindAgentForm.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/2/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class FindAgentForm: UIViewController {
    
    let countryView = TabView()
    let button = CustomButton(buttonName: "Find Agent",bgColor: #colorLiteral(red: 0.1685836613, green: 0.3353172839, blue: 0.7531601191, alpha: 1), textColor: .white)
    
    lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.text = "Enter address so that we find you an agent"
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Find an agent"
        configureTitleLabel()
        configureCountryView()
        configureButton()
    }
    
    func configureTitleLabel() {
        view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
        ])
    }
    
    func configureCountryView() {
        view.addSubview(countryView)
        countryView.titleLabel.text = "Country"
        countryView.answerText.text = "Japan"
        countryView.downArrow.addTarget(self, action: #selector(openPopUP), for: .touchUpInside)
        countryView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            countryView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            countryView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            countryView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15)
        ])
    }
    
    @objc func openPopUP() {
        print("i am modal")
    }
    
    func configureButton() {
        view.addSubview(button)
        button.addTarget(self, action: #selector(navTo), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            button.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            button.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
            button.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
   @objc func navTo() {
    navigationController?.pushViewController(SearchAgentResult(), animated: true)
    }
}
