//
//  SearchAgentResult.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/2/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

struct Agent {
    var name: String
    var image: UIImage
    var category: String
    var star: String
}

class SearchAgentResult: UIViewController {
    
    let agentData: [Agent] = [Agent(name: "Ryoko Yonekura", image: #imageLiteral(resourceName: "men1"), category: "Leader", star: "4"), Agent(name: "Bibek Ji", image: #imageLiteral(resourceName: "men2"), category: "Agent", star: "5"), Agent(name: "Pooja Lama", image: #imageLiteral(resourceName: "female2"), category: "Agent", star: "4"), Agent(name: "Agusta Lee", image: #imageLiteral(resourceName: "female1"), category: "Agent", star: "4")]
    
    lazy var tableView: UITableView = {
       let tv = UITableView()
        return tv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Find an agent"
        configureTableView()
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(AgentCell.self, forCellReuseIdentifier: "agentCell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15)
        ])
    }
}

extension SearchAgentResult: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return agentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "agentCell", for: indexPath) as! AgentCell
        cell.setUpData(image: agentData[indexPath.row].image, name: agentData[indexPath.row].name, category: agentData[indexPath.row].category, star: agentData[indexPath.row].star)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let apc = AgentProfile()
        apc.name = agentData[indexPath.row].name
        apc.image = agentData[indexPath.row].image
        navigationController?.pushViewController(apc, animated: true)
    }
    
}


class AgentCell: UITableViewCell {
    
    lazy var AgentImage: UIImageView = {
       let image = UIImageView()
        image.layer.cornerRadius = 40
        image.clipsToBounds = true
        return image
    }()
    
    lazy var agentName: UILabel = {
       let label = UILabel()
        return label
    }()
    
    lazy var agentCategory: UILabel = {
       let label = UILabel()
        return label
    }()
    
    lazy var starLabel: UILabel = {
       let label = UILabel()
        return label
    }()
    
    lazy var stackView: UIStackView = {
       let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fillEqually
        sv.addArrangedSubview(agentName)
        sv.addArrangedSubview(agentCategory)
        
        let reviewStack = UIStackView()
        reviewStack.axis = .horizontal
        reviewStack.distribution = .fill
        reviewStack.spacing = 8

        sv.addArrangedSubview(reviewStack)
        reviewStack.translatesAutoresizingMaskIntoConstraints = false
        reviewStack.widthAnchor.constraint(equalToConstant: 80).isActive = true


        let starImage = UIImageView(image: UIImage(named: "star"))
        starImage.contentMode = .scaleAspectFit

        let label = UILabel()
        let count = self.starLabel.text
        label.text = "\(count ?? "5")/5"

        let reviewLabel = UILabel()
        reviewLabel.text = "(20 reviews)"
        reviewLabel.textColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)

        reviewStack.addArrangedSubview(starImage)
        starImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            starImage.heightAnchor.constraint(equalToConstant: 15),
            starImage.widthAnchor.constraint(equalToConstant: 15)
        ])

        reviewStack.addArrangedSubview(label)
        reviewStack.addArrangedSubview(reviewLabel)
       
        return sv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureAgentImage()
        configureStackView()
 
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureStackView() {
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: AgentImage.trailingAnchor, constant: 15),
            stackView.heightAnchor.constraint(equalToConstant: 80),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func configureAgentImage() {
        contentView.addSubview(AgentImage)
        AgentImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            AgentImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            AgentImage.leadingAnchor.constraint(equalTo: leadingAnchor),
            AgentImage.heightAnchor.constraint(equalToConstant: 80),
            AgentImage.widthAnchor.constraint(equalToConstant: 80),
            
        ])
    }

    
    func setUpData(image: UIImage?, name: String?, category: String?, star: String?) {
        guard let agentImage = image else {return}
        guard let name = name else {return}
        guard let category = category else {return}
        guard let star = star else {return}
        AgentImage.image = agentImage
        agentName.text = name
        agentCategory.text = category
        starLabel.text = star
  
    }
}
