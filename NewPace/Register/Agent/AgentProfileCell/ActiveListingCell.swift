//
//  ActiveListingCell.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/3/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class ActiveListingCell: UITableViewCell {
    

    
    lazy var propertyImage: UIImageView = {
       let image = UIImageView()
        image.contentMode = .scaleAspectFit
        return image
    }()
    
    lazy var propertyName: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    lazy var propertyLocation: UILabel = {
         let label = UILabel()
         label.font = UIFont.systemFont(ofSize: 15)
          return label
      }()
      
    lazy var propertyPrice: UILabel = {
         let label = UILabel()
         label.font = UIFont.systemFont(ofSize: 15)
          return label
      }()
      
    
    
    lazy var stackView: UIStackView = {
       let sv = UIStackView()
        sv.axis = .vertical
        sv.distribution = .fillEqually
        sv.addArrangedSubview(propertyName)
        sv.addArrangedSubview(propertyLocation)
        sv.addArrangedSubview(propertyPrice)
        return sv
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configurePropertyImage()
        configureStackView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configurePropertyImage() {
        contentView.addSubview(propertyImage)
        propertyImage.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            propertyImage.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            propertyImage.heightAnchor.constraint(equalToConstant: 80),
            propertyImage.widthAnchor.constraint(equalToConstant: 80)
        ])
    }
    
    func configureStackView() {
        contentView.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: propertyImage.trailingAnchor, constant: 10),
            stackView.heightAnchor.constraint(equalToConstant: 80),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func setData(image: UIImage?, name: String?, location: String?, price: String?) {
        guard let image = image else {return}
        guard let name = name else {return}
        guard let location = location else {return}
        guard let price = price else {return}
        
        propertyImage.image = image
        propertyName.text = name
        propertyLocation.text = location
        propertyPrice.text = price
    }
}
