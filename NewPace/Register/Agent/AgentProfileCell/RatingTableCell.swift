//
//  RatingTableCell.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/3/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit


class RatingTableCell: UITableViewCell {
    
    lazy var reviewerName: UILabel = {
       let label = UILabel()
       label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    lazy var reviewedDate: UILabel = {
       let label = UILabel()
       label.font = UIFont.systemFont(ofSize: 10)
        return label
    }()
    
    lazy var reviewrDescription: UILabel = {
       let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .justified
        return label
    }()
    
    lazy var starStackView: UIStackView = {
       let stackview = UIStackView()
        stackview.axis = .horizontal
        stackview.distribution = .fill
        stackview.spacing = 3
        let imageView = UIImageView(image: #imageLiteral(resourceName: "star"))
        imageView.contentMode = .scaleAspectFit
        imageView.image = imageView.image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = #colorLiteral(red: 0.2501999438, green: 0.7749417424, blue: 0.7332472801, alpha: 1)
        stackview.addArrangedSubview(imageView)
        
        let starCountLabel = UILabel()
        starCountLabel.text = "5/5"
        starCountLabel.font = UIFont.systemFont(ofSize: 15)
        stackview.addArrangedSubview(starCountLabel)
        
        return stackview
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureReviewerLabel()
        configureReviewedDate()
        configureReviewerDescription()
        configureStarStackView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureReviewerLabel() {
        contentView.addSubview(reviewerName)
        reviewerName.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            reviewerName.topAnchor.constraint(equalTo: topAnchor),
            reviewerName.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
    }
    
    func configureReviewedDate() {
        contentView.addSubview(reviewedDate)
        reviewedDate.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            reviewedDate.topAnchor.constraint(equalTo: reviewerName.bottomAnchor, constant: 5),
            reviewedDate.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
    }
    
    func configureReviewerDescription() {
        contentView.addSubview(reviewrDescription)
        reviewrDescription.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            reviewrDescription.topAnchor.constraint(equalTo: reviewedDate.bottomAnchor, constant: 10),
            reviewrDescription.leadingAnchor.constraint(equalTo: leadingAnchor),
            reviewrDescription.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    func configureStarStackView() {
        contentView.addSubview(starStackView)
        starStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            starStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            starStackView.heightAnchor.constraint(equalToConstant: 20),
            starStackView.widthAnchor.constraint(equalToConstant: 60)
        ])
    }
    
    
    func setData(name: String?, date: String?, description: String?) {
        guard let name = name else {return}
        guard let date = date else {return}
        guard let description = description else {return}
        
        reviewerName.text = name
        reviewedDate.text = date
        reviewrDescription.text = description
    }
}

