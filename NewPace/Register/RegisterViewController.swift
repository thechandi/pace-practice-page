//
//  RegisterViewController.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/1/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    lazy var backgroundImageView: UIView = {
       let iv = UIImageView()
        iv.image = UIImage(named: "home")
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    lazy var blueView: UIView = {
       let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.09803921569, green: 0.2980392157, blue: 0.7058823529, alpha: 1)
        view.alpha = 0.7
       return view
    }()
    
    lazy var text1: UILabel = {
       let label = UILabel()
        label.text = "Do you want to rent or sale your Home?"
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var text2: UILabel = {
       let label = UILabel()
        label.text = "List your property for selling and make lot of savings"
        label.textColor = .white
        label.textAlignment = .center
        label.numberOfLines = 0
        return label
    }()
    
    lazy var findAgentBtn: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Find an agent", for: .normal)
        button.tintColor = #colorLiteral(red: 0.09803921569, green: 0.2980392157, blue: 0.7058823529, alpha: 1)
        button.layer.cornerRadius = 50 / 2
        button.backgroundColor = .white
       return button
    }()
    
    lazy var registerButton: UIButton = {
           let button = UIButton(type: .system)
           button.setTitle("Register a property", for: .normal)
           button.tintColor = #colorLiteral(red: 0.09803921569, green: 0.2980392157, blue: 0.7058823529, alpha: 1)
           button.layer.cornerRadius = 50 / 2
           button.backgroundColor = .white
          return button
       }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        configureNavigationBar()
        configureBackgroundImage()
        configureBlueView()
        configureText2()
        configureText1()
        configureRegisterButton()
        configureAgentButton()
       
    }
    
    func configureNavigationBar() {
        navigationItem.title = "Register a Property / Room"
    }
    
    func configureBackgroundImage() {
        view.addSubview(backgroundImageView)
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            backgroundImageView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            backgroundImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    func configureBlueView() {
        view.addSubview(blueView)
        blueView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
           blueView.topAnchor.constraint(equalTo: view.topAnchor),
           blueView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
           blueView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
           blueView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }
    
    func configureText2() {
        view.addSubview(text2)
        text2.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            text2.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            text2.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            text2.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            text2.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50)
        ])
    }
    
    func configureText1() {
        view.addSubview(text1)
        text1.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            text1.bottomAnchor.constraint(equalTo: text2.topAnchor, constant: -30),
            text1.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            text1.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            text1.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50)
          
        ])
    }
    
    func configureAgentButton() {
        view.addSubview(findAgentBtn)
        findAgentBtn.translatesAutoresizingMaskIntoConstraints = false
        findAgentBtn.addTarget(self, action: #selector(findAgentButtonTap), for: .touchUpInside)
        NSLayoutConstraint.activate([
            findAgentBtn.bottomAnchor.constraint(equalTo: registerButton.topAnchor, constant: -20),
            findAgentBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            findAgentBtn.heightAnchor.constraint(equalToConstant: 50),
            findAgentBtn.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
            findAgentBtn.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10)
        ])
    }
    
    func configureRegisterButton() {
        view.addSubview(registerButton)
        registerButton.addTarget(self, action: #selector(registerButtonTap), for: .touchUpInside)
        registerButton.translatesAutoresizingMaskIntoConstraints = false
          NSLayoutConstraint.activate([
              registerButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -40),
              registerButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
              registerButton.heightAnchor.constraint(equalToConstant: 50),
              registerButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10),
              registerButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10)
          ])
    }
    
    @objc func findAgentButtonTap() {
        navigationController?.pushViewController(FindAgentController(), animated: true)
    }
    
    @objc func registerButtonTap() {
        navigationController?.pushViewController(RegisterProperty1(), animated: true)
    }
}
