//
//  TabView.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/1/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class TabView: UIView {
    
    var answerText = UILabel()
    var downArrow = UIButton(type: .system)

    lazy var  titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        return label
    }()
  
    
    lazy var answerView: UIView = {
        let view = UIView()
        answerText.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.addSubview(answerText)
        answerText.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            answerText.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            answerText.leadingAnchor.constraint(equalTo: view.leadingAnchor)
        ])
        downArrow.setImage(UIImage(named: "downarrow"), for: .normal)
        downArrow.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        view.addSubview(downArrow)
        downArrow.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            downArrow.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            downArrow.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            downArrow.heightAnchor.constraint(equalToConstant: 15),
            downArrow.widthAnchor.constraint(equalToConstant: 15)
        ])
        
        let singleLine = UIView()
        singleLine.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        view.addSubview(singleLine)
        singleLine.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            singleLine.topAnchor.constraint(equalTo: view.bottomAnchor),
            singleLine.heightAnchor.constraint(equalToConstant: 0.5),
            singleLine.widthAnchor.constraint(equalTo: view.widthAnchor)
        ])
        return view
    }()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureTitleLabel()
        configureAnswerView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureTitleLabel() {
        self.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
        
        ])
//        titleLabel.translatesAutoresizingMaskIntoConstraints = false
      
    }
    
    func configureAnswerView() {
        self.addSubview(answerView)
        answerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            answerView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
            answerView.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor),
            answerView.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor),
            answerView.heightAnchor.constraint(equalToConstant: 30)
        ])
    }
    

}
