//
//  CustomButton.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/2/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
 
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(buttonName: String, bgColor: UIColor, textColor: UIColor ) {
        super.init(frame: .zero)
        setTitle(buttonName, for: .normal)
        backgroundColor = bgColor
        tintColor = textColor
        layer.cornerRadius = 25
    
    }

}
