//
//  TickCustomButton.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/3/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class TickCustmButton: UIView {
    
    lazy var button: UIButton = {
        let button = UIButton(type: .system)
       // button.setTitle("A", for: .normal)
        button.setImage(#imageLiteral(resourceName: "untick"), for: .normal)
        button.tintColor = .black
        button.isUserInteractionEnabled = true
        return button
    }()
    
    lazy var buttonTitle: UILabel  = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 15)
        label.textColor = .black
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(buttonTitle: String) {
        super.init(frame: .zero)
        self.buttonTitle.text = buttonTitle
        configureButton()
        configureTitle()
    }
    
    func configureButton() {
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            button.heightAnchor.constraint(equalToConstant: 15),
            button.widthAnchor.constraint(equalToConstant: 15)
        ])
    }
    
    func configureTitle() {
        self.addSubview(buttonTitle)
        buttonTitle.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            buttonTitle.leadingAnchor.constraint(equalTo: button.trailingAnchor, constant: 15),
            
        ])
    }
}


//git remote add origin https://thechandi@gitlab.com/thechandi/pace-practice-page.git
