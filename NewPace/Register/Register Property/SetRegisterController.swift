//
//  SetRegisterController.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/1/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class SetRegisterController: UIViewController {
    
    var country: TabView = TabView()
    var zipCode: TabView = TabView()
    
    lazy var nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Next", for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        button.tintColor = .white
        button.layer.cornerRadius = 50 / 2
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Register a property"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save progress", style: .plain, target: self, action: #selector(saveProgressTap))
    
        configureFirst()
        configureSecond()
        configureNextButton()
    }
    
    @objc func saveProgressTap() {
        print("save progress tap")
    }
    
    func configureFirst() {
        view.addSubview(country)
        country.titleLabel.text = "Country"
        country.answerText.text = "Japan"
        country.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            country.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            country.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            country.heightAnchor.constraint(equalToConstant: 30),
            country.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15 )
        ])
    }
    
    func configureSecond() {
        view.addSubview(zipCode)
        zipCode.titleLabel.text = "Zip Code"
        zipCode.answerText.text = "e.g 123-0862"
        zipCode.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            zipCode.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15),
            zipCode.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15),
            zipCode.topAnchor.constraint(equalTo: country.bottomAnchor, constant: 45),
            zipCode.heightAnchor.constraint(equalToConstant: 30),
        ])
    }
    
    func configureNextButton() {
        view.addSubview(nextButton)
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nextButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20),
            nextButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            nextButton.heightAnchor.constraint(equalToConstant: 50),
            nextButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15)
        ])
    }
    

}
