//
//  RegisterProperty2.swift
//  NewPace
//
//  Created by ENFINY INNOVATIONS on 4/3/20.
//  Copyright © 2020 ENFINY INNOVATIONS. All rights reserved.
//

import UIKit

class RegisterProperty2: UIViewController {
    
    let button1 = TickCustmButton(buttonTitle: "For sale")
    let button2 = TickCustmButton(buttonTitle: "For rent")
    let nextButton = CustomButton(buttonName: "Next", bgColor: #colorLiteral(red: 0.1685836613, green: 0.3353172839, blue: 0.7531601191, alpha: 1), textColor: .white)
    
    lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.text = "What do you want to register your property for?"
        label.numberOfLines = 0
        label.font = UIFont.systemFont(ofSize: 15)
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationItem.title = "Register a property"
        configureTitleLabel()
        configureButton1()
        configureButton2()
        configureNextButton()
    }
    
    func configureTitleLabel() {
        view.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15),
            titleLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            titleLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15)
        ])
    }

      func configureButton1() {
          view.addSubview(button1)
 
          button1.translatesAutoresizingMaskIntoConstraints = false
          NSLayoutConstraint.activate([
              button1.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
              button1.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
              button1.heightAnchor.constraint(equalToConstant: 30),
    
          ])
      }
    
    func configureButton2() {
        view.addSubview(button2)
        button2.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            button2.topAnchor.constraint(equalTo: button1.bottomAnchor, constant: 5),
            button2.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            button2.heightAnchor.constraint(equalToConstant: 30),
           
        ])
    }
    
    
    func configureNextButton() {
        view.addSubview(nextButton)
        nextButton.addTarget(self, action: #selector(nextButtonTap), for: .touchUpInside)
        nextButton.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            nextButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -15),
            nextButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15),
            nextButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15),
            nextButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    @objc func nextButtonTap() {
        navigationController?.pushViewController(SetRegisterController(), animated: true)
    }
}

